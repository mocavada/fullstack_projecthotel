package com.mochotel.fullstack.repository;

import com.mochotel.fullstack.entity.RoomEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PageableRoomRepository extends PagingAndSortingRepository<RoomEntity, Long> {

    RoomEntity findById(Long id, Pageable page);

}
