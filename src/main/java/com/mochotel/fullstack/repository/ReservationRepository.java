package com.mochotel.fullstack.repository;

import com.mochotel.fullstack.entity.ReservationEntity;
import org.springframework.data.repository.CrudRepository;

public interface ReservationRepository extends CrudRepository<ReservationEntity, Long> {

}