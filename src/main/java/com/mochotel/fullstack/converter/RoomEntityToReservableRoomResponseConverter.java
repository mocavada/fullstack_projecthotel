package com.mochotel.fullstack.converter;

import org.springframework.core.convert.converter.Converter;

import com.mochotel.fullstack.entity.RoomEntity;
import com.mochotel.fullstack.model.Links;
import com.mochotel.fullstack.model.Self;
import com.mochotel.fullstack.model.response.ReservableRoomResponse;
import com.mochotel.fullstack.rest.ResourceConstants;

public class RoomEntityToReservableRoomResponseConverter implements Converter<RoomEntity, ReservableRoomResponse>{

    @Override
    public ReservableRoomResponse convert(RoomEntity source) {


        ReservableRoomResponse reservationResponse = new ReservableRoomResponse();
        reservationResponse.setRoomNumber(source.getRoomNumber());
        reservationResponse.setPrice( Integer.valueOf(source.getPrice()) );

        Links links = new Links();
        Self self = new Self();
        self.setRef(ResourceConstants.ROOM_RESERVATION_V1 + "/" + source.getId());
        links.setSelf(self);

        reservationResponse.setLinks(links);

        return reservationResponse;
    }



}

