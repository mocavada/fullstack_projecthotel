package com.mochotel.fullstack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class MocHotelApplication {

	public static void main(String[] args) {
		SpringApplication.run(MocHotelApplication.class, args);
	}
}
