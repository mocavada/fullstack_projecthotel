package com.mochotel.fullstack.model;

public class Links {

    private Self self;

    public Self getSelf() {
        return self;
    }

    public void setSelf(Self self) {
        this.self = self;
    }
}
