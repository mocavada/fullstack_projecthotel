package com.mochotel.fullstack.config;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableJpaRepositories("com.mochotel.fullstack.repository")
@EnableTransactionManagement
public class DatabaseConfig {

}
